package com.epam.training.util;

import com.epam.training.beans.User;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public class MyInvocationHandler implements InvocationHandler {

    private static final Logger LOGGER = Logger.getLogger(MyInvocationHandler.class.getName());
    private static final String METHOD_NAME = "getField";
    private static final String NAME = "name";

    private Object target;

    public MyInvocationHandler(Object object) {
        this.target = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals(METHOD_NAME)){
            Field fields = null;
            Class aClass = User.class;
            try {
                fields = aClass.getDeclaredField(NAME);
            } catch (RuntimeException | NoSuchFieldException ex) {
                LOGGER.error("User field was not found", ex);
            }
            return fields;
        }
        return method.invoke(target, args);
    }
}