package com.epam.training.util;

import com.epam.training.annotations.MyEqual;
import com.epam.training.enums.CompareBy;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;

public class EqualityAnalyzer {
    private static final Logger LOGGER = Logger.getLogger(EqualityAnalyzer.class.getName());

    /**
     * Methot to compare fields according to enum fields
     *
     * @param objectOne - object to get field to compare
     * @param objectTwo - object to get field to compare
     * @return - is fields equals (true/false)
     */
    public boolean equalObjects(Object objectOne, Object objectTwo) {
        boolean equalityResult = true;
        Field[] fields = objectOne.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object valOne = field.get(objectOne);
                Object valTwo = field.get(objectTwo);
                if (field.isAnnotationPresent(MyEqual.class)) {
                    if (field.getAnnotation(MyEqual.class).compareBy().equals(CompareBy.REFERENCE)) {
                        if (valOne != valTwo)
                            equalityResult = false;
                    } else if (field.getAnnotation(MyEqual.class).compareBy().equals(CompareBy.VALUE)) {
                           if (!valOne.equals(valTwo))
                            equalityResult = false;
                    }
                    if (equalityResult = false)
                        break;
                }
                field.setAccessible(false);
            } catch (IllegalAccessException ex) {
                LOGGER.error("Something wrong in EqualityAnalyzer/equalObjects(Object objectOne, Object objectTwo)", ex);
            }
        }
        return equalityResult;
    }
}