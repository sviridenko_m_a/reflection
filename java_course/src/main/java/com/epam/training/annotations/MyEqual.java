package com.epam.training.annotations;

import com.epam.training.enums.CompareBy;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)

/**
 * Annotation to mark fields, using enum (CompareBy)
 */
public @interface MyEqual {
    CompareBy[] compareBy();
}