package com.epam.training.factories;

import com.epam.training.interfaces.ConnectionInterface;
import com.epam.training.util.MyInvocationHandler;
import com.epam.training.annotations.MyProxy;
import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;

public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static ConnectionInterface connectorInterface;

    /**
     * Method build & return proxy object.
     * @param object - class object to make proxy
     * @return - proxy object
     */
    public static ConnectionInterface getInstateOf(Object object) {
        if (object.getClass().isAnnotationPresent(MyProxy.class)) {
            try {
                ClassLoader objectClassLoader = object.getClass().getClassLoader();
                Class[] objectInterfaces = object.getClass().getInterfaces();
                connectorInterface = (ConnectionInterface)
                        Proxy.newProxyInstance(objectClassLoader, objectInterfaces, new MyInvocationHandler(object));
            } catch (RuntimeException ex) {
                LOGGER.error("Proxy object has not built (ConnectionFactory/getInstateOf(Object object))", ex);
            }
        }
        return connectorInterface;
    }
}