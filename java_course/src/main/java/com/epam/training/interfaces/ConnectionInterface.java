package com.epam.training.interfaces;

import java.lang.reflect.Field;

public interface ConnectionInterface {
    Field getField();
}
