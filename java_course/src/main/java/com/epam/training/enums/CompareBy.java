package com.epam.training.enums;

/**
 * Enum constants uses to mark fields to compare in EqualityAnalyzer
 */
public enum CompareBy {
    REFERENCE, VALUE
}
