package com.epam.training.impls;

import com.epam.training.beans.User;
import com.epam.training.enums.CompareBy;
import com.epam.training.interfaces.ConnectionInterface;
import com.epam.training.annotations.MyProxy;
import com.epam.training.annotations.MyEqual;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Objects;

@MyProxy
public class MySQLConnector implements ConnectionInterface {

    private static final Logger LOGGER = Logger.getLogger(OracleConnector.class.getName());

    private Class<User> aClass = null;
    @MyEqual(compareBy = {CompareBy.VALUE, CompareBy.REFERENCE})
    private User user;
    @MyEqual(compareBy = CompareBy.REFERENCE)
    private String str;
    @MyEqual(compareBy = CompareBy.VALUE)
    private Integer nt;

    public MySQLConnector() {

    }

    public MySQLConnector(User user, String str, Integer nt) {
        try {
            this.user = user;
            this.str = str;
            this.nt = nt;
            aClass = User.class;
        } catch (RuntimeException ex) {
            LOGGER.error("In MySQLConnector constructor", ex);
        }
    }

    /**
     * Method to get field 'name' from class User
     *
     * @return - field name
     */
    @Override
    public Field getField() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MySQLConnector that = (MySQLConnector) o;
        return Objects.equals(aClass, that.aClass) &&
                Objects.equals(user, that.user) &&
                Objects.equals(str, that.str) &&
                Objects.equals(nt, that.nt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aClass, user, str, nt);
    }
}