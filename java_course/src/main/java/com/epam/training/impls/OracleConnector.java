package com.epam.training.impls;

import com.epam.training.annotations.MyEqual;
import com.epam.training.beans.User;
import com.epam.training.enums.CompareBy;
import com.epam.training.interfaces.ConnectionInterface;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Objects;

public class OracleConnector implements ConnectionInterface {

    private static final Logger LOGGER = Logger.getLogger(OracleConnector.class.getName());

    private Class<User> aClass = null;
    @MyEqual(compareBy = CompareBy.REFERENCE)
    private User user;
    @MyEqual(compareBy = CompareBy.REFERENCE)
    private String str;
    @MyEqual(compareBy = CompareBy.VALUE)
    private Integer nt;

    public OracleConnector(){
    }

    public OracleConnector(User user, String str, Integer nt) {
        try {
            this.user = user;
            this.str = str;
            this.nt = nt;
            aClass = User.class;
        } catch (RuntimeException ex) {
            LOGGER.error("In OracleConnector constructor", ex);
        }
    }

    /**
     * Method to get field 'name' from class User
     * @return - field name
     */
    @Override
    public Field getField() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OracleConnector that = (OracleConnector) o;
        return Objects.equals(aClass, that.aClass) &&
                Objects.equals(user, that.user) &&
                Objects.equals(str, that.str) &&
                Objects.equals(nt, that.nt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aClass, user, str, nt);
    }
}