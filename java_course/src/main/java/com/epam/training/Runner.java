package com.epam.training;

import com.epam.training.beans.User;
import com.epam.training.factories.ConnectionFactory;
import com.epam.training.impls.MySQLConnector;
import com.epam.training.impls.OracleConnector;
import com.epam.training.util.EqualityAnalyzer;
import org.apache.log4j.Logger;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {

        EqualityAnalyzer equalityAnalyzer = new EqualityAnalyzer();
        User user = new User("Misha", 1234);
        String str = "aa";
        Integer v = 22;

        LOGGER.info(new MySQLConnector(user, str, v).equals(new MySQLConnector(user, str, v)));
        LOGGER.info(new OracleConnector(user, str, v).equals(new OracleConnector(user, str, v))+"\n");

        LOGGER.info(new MySQLConnector(user, str, v).equals(ConnectionFactory.getInstateOf(new MySQLConnector(user, str, v))));
        LOGGER.info(new OracleConnector(user, str, v).equals(ConnectionFactory.getInstateOf(new OracleConnector(user, str, v)))+"\n");

        LOGGER.info(equalityAnalyzer.equalObjects(new MySQLConnector(user, str, v), new MySQLConnector(user, str, v)));
        LOGGER.info(equalityAnalyzer.equalObjects(new MySQLConnector(user, str, v), new MySQLConnector(user, str, v)));
        LOGGER.info(equalityAnalyzer.equalObjects(new MySQLConnector(user, str, v), new MySQLConnector(user, str, v))+"\n");

        LOGGER.info(equalityAnalyzer.equalObjects(new OracleConnector(user, str, v), new OracleConnector(user, str, v)));
        LOGGER.info(equalityAnalyzer.equalObjects(new OracleConnector(user, str, v), new OracleConnector(user, str, v)));
        LOGGER.info(equalityAnalyzer.equalObjects(new OracleConnector(user, str, v), new OracleConnector(user, str, v))+"\n");

        LOGGER.info(new MySQLConnector().getField());
        LOGGER.info(ConnectionFactory.getInstateOf(new MySQLConnector()).getField());
    }
}

