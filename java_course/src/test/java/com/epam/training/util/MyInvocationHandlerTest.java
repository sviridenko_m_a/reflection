package com.epam.training.util;

import com.epam.training.factories.ConnectionFactory;
import com.epam.training.impls.MySQLConnector;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

public class MyInvocationHandlerTest {

    @Test
    public void invoke() {
        Field axpect = ConnectionFactory.getInstateOf(new MySQLConnector()).getField();
        assertNotNull(ConnectionFactory.getInstateOf(new MySQLConnector()).getField());
        assertEquals(axpect, ConnectionFactory.getInstateOf(new MySQLConnector()).getField());
    }
}