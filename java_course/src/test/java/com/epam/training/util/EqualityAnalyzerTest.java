package com.epam.training.util;

import com.epam.training.beans.User;
import com.epam.training.impls.MySQLConnector;
import com.epam.training.impls.OracleConnector;
import org.junit.Test;

import static org.junit.Assert.*;

public class EqualityAnalyzerTest {

    @Test
    public void equalObjects() {
        EqualityAnalyzer equalityAnalyzer = new EqualityAnalyzer();

        User user = new User("Linda", 123);
        MySQLConnector mySQLConnector = new MySQLConnector(user, "lala", 54);
        MySQLConnector mySQLConnector1 = new MySQLConnector(user, "lala", 54);
        MySQLConnector mySQLConnector2 = new MySQLConnector(user, "la", 45);

        assertTrue(equalityAnalyzer.equalObjects(mySQLConnector, mySQLConnector1));
        assertFalse(equalityAnalyzer.equalObjects(mySQLConnector, mySQLConnector2));
    }
}