package com.epam.training.factories;

import com.epam.training.beans.User;
import com.epam.training.impls.MySQLConnector;
import com.epam.training.interfaces.ConnectionInterface;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConnectorFactoryTest {

    @Test
    public void getInstateOf() {
        User user = new User("Linda", 123);
        MySQLConnector mySQLConnector = new MySQLConnector(user, "lala", 54);
        assertNotNull(ConnectionFactory.getInstateOf(mySQLConnector));

        ConnectionInterface MySQLInterface = new MySQLConnector(user, "lala", 54);
        assertEquals(ConnectionFactory.getInstateOf(mySQLConnector), MySQLInterface);
    }
}